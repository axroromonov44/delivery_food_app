import 'package:delivery_food_app/models/category_model.dart';
import 'package:flutter/material.dart';

class CustomCategoryFilter extends StatelessWidget {
  final List<CategoryModel> categoryModel;

  const CustomCategoryFilter({
    Key? key,
    required this.categoryModel,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
        shrinkWrap: true,
        itemCount: categoryModel.length,
        itemBuilder: (context, index) {
          return Container(
            width: double.infinity,
            margin: const EdgeInsets.only(top: 10),
            padding: const EdgeInsets.symmetric(
              horizontal: 30,
              vertical: 10,
            ),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(5.0),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  categoryModel[index].name,
                  style: Theme.of(context).textTheme.headline4,
                ),
                SizedBox(
                  height: 25,
                  child: Checkbox(
                    value: false,
                    onChanged: (bool? newValue) {},
                  ),
                )
              ],
            ),
          );
        });
  }
}
