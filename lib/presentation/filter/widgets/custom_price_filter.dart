import 'package:delivery_food_app/models/price_model.dart';
import 'package:flutter/material.dart';

class CustomPriceFilter extends StatelessWidget {
  final List<PriceModel> priceModel;

  const CustomPriceFilter({Key? key, required this.priceModel})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: priceModel
          .map((price) => InkWell(
                onTap: () {},
                child: Container(
                  margin: const EdgeInsets.only(top: 10, bottom: 10),
                  padding: const EdgeInsets.symmetric(
                    horizontal: 40,
                    vertical: 10,
                  ),
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(5),
                  ),
                  child: Text(
                    price.price,
                    style: Theme.of(context).textTheme.headline4,
                  ),
                ),
              ))
          .toList(),
    );
  }
}
