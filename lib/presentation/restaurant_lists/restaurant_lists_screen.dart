import 'package:delivery_food_app/models/restaurant_model.dart';
import 'package:delivery_food_app/presentation/home/widget/restaurant_card.dart';
import 'package:flutter/material.dart';

class RestaurantListsScreen extends StatelessWidget {
  final List<RestaurantModel> restaurantModel;

  const RestaurantListsScreen({Key? key, required this.restaurantModel})
      : super(key: key);
  static const String routeName = '/restaurant-lists';

  static Route route({required List<RestaurantModel> restaurantModel}) {
    return MaterialPageRoute(
      builder: (_) => RestaurantListsScreen(
        restaurantModel: restaurantModel,
      ),
      settings: RouteSettings(name: routeName),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Restaurants'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: ListView.builder(
          itemCount: restaurantModel.length,
          itemBuilder: (context, index) {
            return RestaurantCard(
                onTap: () {}, restaurantModel: restaurantModel[index]);
          },
        ),
      ),
    );
  }
}
