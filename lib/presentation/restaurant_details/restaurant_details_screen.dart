import 'package:delivery_food_app/models/restaurant_model.dart';
import 'package:delivery_food_app/presentation/restaurant_details/widget/restaurant_information.dart';
import 'package:flutter/material.dart';

class RestaurantDetailsScreen extends StatelessWidget {
  final RestaurantModel restaurantModel;

  const RestaurantDetailsScreen({Key? key, required this.restaurantModel})
      : super(key: key);
  static const String routeName = '/restaurant-details';

  static Route route({required RestaurantModel restaurantModel}) {
    return MaterialPageRoute(
      builder: (_) => RestaurantDetailsScreen(
        restaurantModel: restaurantModel,
      ),
      settings: RouteSettings(name: routeName),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
      ),
      extendBodyBehindAppBar: true,
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              height: 220,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.vertical(
                  bottom: Radius.elliptical(
                    MediaQuery.of(context).size.width,
                    50,
                  ),
                ),
                image: DecorationImage(
                  fit: BoxFit.cover,
                  image: AssetImage('assets/burak.jpg'),
                ),
              ),
            ),
            RestaurantInformation(
              restaurantModel: restaurantModel,
            ),
            ListView.builder(
                padding: EdgeInsets.zero,
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                itemCount: restaurantModel.tags.length,
                itemBuilder: (context, index) {
                  return _buildMenuItems(restaurantModel, context, index);
                })
          ],
        ),
      ),
      bottomNavigationBar: BottomAppBar(
        child: Container(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              ElevatedButton(
                style: ElevatedButton.styleFrom(
                  primary: Theme.of(context).primaryColor,
                  shape: RoundedRectangleBorder(),
                  padding: const EdgeInsets.symmetric(horizontal: 50),
                ),
                onPressed: () {},
                child: const Text('Basket'),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildMenuItems(
      RestaurantModel restaurantModel, BuildContext context, int index) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.symmetric(
            horizontal: 20,
            vertical: 10,
          ),
          child: Text(
            restaurantModel.tags[index],
            style: Theme.of(context)
                .textTheme
                .headline3!
                .copyWith(color: Theme.of(context).primaryColor),
          ),
        ),
        Column(
          children: restaurantModel.menuItems
              .where((menuItem) =>
                  menuItem.category == restaurantModel.tags[index])
              .map(
                (menuItem) => Column(
                  children: [
                    Container(
                      color: Colors.white,
                      padding: EdgeInsets.symmetric(horizontal: 20),
                      child: ListTile(
                        dense: true,
                        contentPadding: EdgeInsets.zero,
                        title: Text(
                          menuItem.name,
                          style: Theme.of(context).textTheme.headline4,
                        ),
                        subtitle: Text(
                          menuItem.description,
                          style: Theme.of(context).textTheme.bodyText1,
                        ),
                        trailing: Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            Text('\$${menuItem.price}',
                                style: Theme.of(context).textTheme.bodyText1),
                            IconButton(
                                onPressed: () {},
                                icon: const Icon(Icons.add_circle))
                          ],
                        ),
                      ),
                    ),
                    const Divider(
                      height: 2,
                    )
                  ],
                ),
              )
              .toList(),
        )
      ],
    );
  }
}
