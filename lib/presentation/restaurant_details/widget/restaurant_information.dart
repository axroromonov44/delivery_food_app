import 'package:delivery_food_app/models/restaurant_model.dart';
import 'package:delivery_food_app/presentation/home/widget/restaurant_card.dart';
import 'package:flutter/material.dart';

class RestaurantInformation extends StatelessWidget {
  final RestaurantModel restaurantModel;

  const RestaurantInformation({Key? key, required this.restaurantModel})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            restaurantModel.name,
            style: Theme.of(context)
                .textTheme
                .headline3!
                .copyWith(color: Theme.of(context).primaryColor),
          ),
          const SizedBox(
            height: 10,
          ),
          RestaurantTags(restaurantModel: restaurantModel),
          const SizedBox(
            height: 5,
          ),
          Text(
            '${restaurantModel.distance}km away - \$${restaurantModel.deliveryFee} delivery fee',
            style: Theme.of(context).textTheme.bodyText1,
          ),
          const SizedBox(
            height: 10,
          ),
          Text(
            'Restaurant Information',
            style: Theme.of(context).textTheme.headline4,
          ),
          const SizedBox(
            height: 5,
          ),
          Text(
            'Lorem ipsum dolor sit amet',
            style: Theme.of(context).textTheme.bodyText1,
          ),
        ],
      ),
    );
  }
}
