import 'package:delivery_food_app/bloc/geolocation/geolocation_bloc.dart';
import 'package:delivery_food_app/bloc/place/place_bloc.dart';
import 'package:delivery_food_app/presentation/location/widgets/gmap.dart';
import 'package:delivery_food_app/presentation/location/widgets/location.dart';
import 'package:delivery_food_app/presentation/location/widgets/save_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class LocationScreen extends StatefulWidget {
  const LocationScreen({Key? key}) : super(key: key);
  static const String routeName = '/location';

  static Route route() {
    return MaterialPageRoute(
      builder: (_) => const LocationScreen(),
      settings: const RouteSettings(name: routeName),
    );
  }

  @override
  State<LocationScreen> createState() => _LocationScreenState();
}

class _LocationScreenState extends State<LocationScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocBuilder<PlaceBloc, PlaceState>(
        builder: (context, state) {
          if (state is PlaceLoadingState) {
            return Stack(
              children: [
                BlocBuilder<GeolocationBloc, GeolocationState>(
                  builder: (context, state) {
                    if (state is GeolocationLoading) {
                      return const Center(
                        child: CircularProgressIndicator(),
                      );
                    } else if (state is GeolocationLoaded) {
                      return SizedBox(
                        height: MediaQuery.of(context).size.height,
                        width: double.infinity,
                        child: GMap(
                          lat: state.position.latitude,
                          lng: state.position.longitude,
                        ),
                      );
                    } else {
                      return const Text('Something went wrong');
                    }
                  },
                ),
                SaveButton(),
                Location()
              ],
            );
          } else if (state is PlaceLoadedState) {
            return Stack(
              children: [
                GMap(
                  lat: state.place.lat,
                  lng: state.place.lon,
                ),
                SaveButton(),
                Location()
              ],
            );
          } else {
            return const Text('Something went wrong');
          }
        },
      ),
    );
  }
}
