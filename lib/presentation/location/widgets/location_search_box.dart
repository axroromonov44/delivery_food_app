import 'package:delivery_food_app/bloc/auto_complete/autocomplete_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';

class LocationSearchBox extends StatelessWidget {
  const LocationSearchBox({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<AutocompleteBloc, AutocompleteState>(
      builder: (context, state) {
        if (state is AutocompleteLoadingState) {
          return const Center(
            child: CircularProgressIndicator(),
          );
        } else if (state is AutocompleteLoadedState) {
          return Padding(
            padding: const EdgeInsets.all(8.0),
            child: TextField(
              decoration: InputDecoration(
                  fillColor: Colors.white,
                  filled: true,
                  hintText: 'Enter your Location',
                  suffixIcon: const Icon(Icons.search),
                  contentPadding:
                      const EdgeInsets.only(left: 20, bottom: 5, right: 5),
                  focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10),
                      borderSide: const BorderSide(color: Colors.white)),
                  enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10),
                      borderSide: const BorderSide(color: Colors.white))),
              onChanged: (value) {
                context
                    .read<AutocompleteBloc>()
                    .add(LoadAutocompleteEvent(searchInput: value));
              },
            ),
          );
        } else {
          return const Text(
            'Something went wrong',
          );
        }
      },
    );
  }
}
