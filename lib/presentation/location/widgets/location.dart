import 'package:delivery_food_app/bloc/auto_complete/autocomplete_bloc.dart';
import 'package:delivery_food_app/bloc/place/place_bloc.dart';
import 'package:delivery_food_app/presentation/location/widgets/location_search_box.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';

class Location extends StatefulWidget {
  const Location({Key? key}) : super(key: key);

  @override
  State<Location> createState() => _LocationState();
}

class _LocationState extends State<Location> {
  @override
  Widget build(BuildContext context) {
    return Positioned(
      top: 40,
      left: 20,
      right: 20,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SvgPicture.asset(
            'assets/logo.svg',
            height: 50,
          ),
          const SizedBox(
            width: 10,
          ),
          Expanded(
            child: Column(
              children: [
                const LocationSearchBox(),
                BlocBuilder<AutocompleteBloc, AutocompleteState>(
                    builder: (context, state) {
                  if (state is AutocompleteLoadingState) {
                    return const Center(
                      child: CircularProgressIndicator(),
                    );
                  } else if (state is AutocompleteLoadedState) {
                    return Container(
                      margin: const EdgeInsets.all(8),
                      height: state.autocomplete.isNotEmpty ? 300 : 0,
                      color: state.autocomplete.isNotEmpty
                          ? Colors.black.withOpacity(0.6)
                          : Colors.transparent,
                      child: ListView.builder(
                        itemCount: state.autocomplete.length,
                        itemBuilder: (context, index) {
                          return ListTile(
                            title: Text(
                              state.autocomplete[index].description,
                              style: Theme.of(context)
                                  .textTheme
                                  .headline6!
                                  .copyWith(
                                    color: Colors.white,
                                  ),
                            ),
                            onTap: () {
                              setState(() {});
                              context.read<PlaceBloc>().add(LoadPlaceEvent(
                                  placeId: state.autocomplete[index].placeId));
                              print(state.autocomplete[index].placeId);
                              print(state);
                            },
                          );
                        },
                      ),
                    );
                  } else {
                    return const Text('Something went wrong');
                  }
                })
              ],
            ),
          ),
        ],
      ),
    );
  }
}
