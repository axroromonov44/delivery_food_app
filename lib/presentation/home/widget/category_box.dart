import 'package:delivery_food_app/models/category_model.dart';
import 'package:delivery_food_app/models/restaurant_model.dart';
import 'package:flutter/material.dart';

class CategoryBox extends StatelessWidget {
  final CategoryModel categoryModel;

  const CategoryBox({Key? key, required this.categoryModel}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final List<RestaurantModel> restaurantModel = RestaurantModel.restaurants
        .where((restaurant) => restaurant.tags.contains(categoryModel.name))
        .toList();
    return InkWell(
      onTap: () {
        Navigator.pushNamed(context, '/restaurant-lists',
            arguments: restaurantModel);
      },
      child: Container(
        width: 80,
        margin: const EdgeInsets.only(right: 5.0),
        decoration: BoxDecoration(
            color: Theme.of(context).primaryColor,
            borderRadius: BorderRadius.circular(5)),
        child: Stack(
          children: [
            Positioned(
              top: 10,
              left: 10,
              child: Container(
                height: 50,
                width: 60,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5),
                    color: Colors.white),
                child: categoryModel.image,
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Align(
                alignment: Alignment.bottomCenter,
                child: Text(
                  categoryModel.name,
                  style: Theme.of(context)
                      .textTheme
                      .headline6!
                      .copyWith(color: Colors.white),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
