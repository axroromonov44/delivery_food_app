import 'package:delivery_food_app/models/restaurant_model.dart';
import 'package:flutter/material.dart';

class RestaurantCard extends StatelessWidget {
  final Function()? onTap;
  final RestaurantModel restaurantModel;

  const RestaurantCard({
    Key? key,
    required this.onTap,
    required this.restaurantModel,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        Navigator.pushNamed(context, '/restaurant-details',
            arguments: restaurantModel);
      },
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Stack(
              children: [
                Container(
                  width: MediaQuery.of(context).size.width,
                  height: 150,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(5),
                      image: DecorationImage(
                          fit: BoxFit.cover,
                          image: AssetImage(restaurantModel.imageUrl))),
                ),
                Positioned(
                  top: 10,
                  right: 10,
                  child: Container(
                    width: 60,
                    height: 30,
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(5)),
                    child: Align(
                      alignment: Alignment.center,
                      child: Text(
                        '${restaurantModel.deliveryTime} min',
                        style: Theme.of(context).textTheme.bodyText1,
                      ),
                    ),
                  ),
                )
              ],
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    restaurantModel.name,
                    style: Theme.of(context).textTheme.headline3,
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  RestaurantTags(
                    restaurantModel: restaurantModel,
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  Text(
                    "${restaurantModel.distance}km - \$${restaurantModel.deliveryFee} delivery fee",
                    style: Theme.of(context).textTheme.bodyText1,
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}

class RestaurantTags extends StatelessWidget {
  final RestaurantModel restaurantModel;

  const RestaurantTags({Key? key, required this.restaurantModel})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: restaurantModel.tags
          .map((tags) => restaurantModel.tags.indexOf(tags) ==
                  restaurantModel.tags.length - 1
              ? Text(
                  tags,
                  style: Theme.of(context).textTheme.bodyText1,
                )
              : Text(
                  '$tags ',
                  style: Theme.of(context).textTheme.bodyText1,
                ))
          .toList(),
    );
  }
}
