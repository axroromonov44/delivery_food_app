import 'package:delivery_food_app/models/category_model.dart';
import 'package:delivery_food_app/models/promo_model.dart';
import 'package:delivery_food_app/models/restaurant_model.dart';
import 'package:delivery_food_app/presentation/home/widget/category_box.dart';
import 'package:delivery_food_app/presentation/home/widget/custom_appbar.dart';
import 'package:delivery_food_app/presentation/home/widget/food_search_box.dart';
import 'package:delivery_food_app/presentation/home/widget/promo_box.dart';
import 'package:delivery_food_app/presentation/home/widget/restaurant_card.dart';
import 'package:delivery_food_app/presentation/restaurant_details/restaurant_details_screen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({Key? key}) : super(key: key);

  static const String routeName = '/';

  static Route route() {
    return MaterialPageRoute(
      builder: (_) => HomeScreen(),
      settings: RouteSettings(name: routeName),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Theme.of(context).backgroundColor,
        appBar: CustomAppbar(),
        body: SingleChildScrollView(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: SizedBox(
                  height: 100,
                  child: ListView.builder(
                      scrollDirection: Axis.horizontal,
                      shrinkWrap: true,
                      itemCount: CategoryModel.categories.length,
                      itemBuilder: (context, index) {
                        return CategoryBox(
                          categoryModel: CategoryModel.categories[index],
                        );
                      }),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: SizedBox(
                  height: 125,
                  child: ListView.builder(
                      scrollDirection: Axis.horizontal,
                      shrinkWrap: true,
                      itemCount: PromoModel.promos.length,
                      itemBuilder: (context, index) {
                        return PromoBox(
                          promoModel: PromoModel.promos[index],
                        );
                      }),
                ),
              ),
              FoodSearchBox(),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Align(
                    alignment: Alignment.topLeft,
                    child: Text(
                      'Top Rated',
                      style: Theme.of(context).textTheme.headline4,
                    )),
              ),
              ListView.builder(
                  shrinkWrap: true,
                  physics: NeverScrollableScrollPhysics(),
                  itemCount: RestaurantModel.restaurants.length,
                  itemBuilder: (context, index) {
                    return RestaurantCard(
                        onTap: () {
                          Navigator.push(
                            context,
                            CupertinoPageRoute(
                              builder: (context) => RestaurantDetailsScreen(
                                restaurantModel:
                                    RestaurantModel.restaurants[index],
                              ),
                            ),
                          );
                        },
                        restaurantModel: RestaurantModel.restaurants[index]);
                  })
            ],
          ),
        ));
  }
}
