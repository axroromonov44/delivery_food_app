import 'package:delivery_food_app/models/category_model.dart';
import 'package:equatable/equatable.dart';

class CategoryFilterModel extends Equatable {
  final int id;
  final CategoryModel categoryModel;
  final bool value;

  const CategoryFilterModel({
    required this.id,
    required this.categoryModel,
    required this.value,
  });

  CategoryFilterModel copyWith({
    int? id,
    CategoryModel? categoryModel,
    bool? value,
  }) {
    return CategoryFilterModel(
      id: id ?? this.id,
      categoryModel: categoryModel ?? this.categoryModel,
      value: value ?? this.value,
    );
  }

  @override
  List<Object?> get props => [
        id,
        categoryModel,
        value,
      ];

  static List<CategoryFilterModel> filters = CategoryModel.categories
      .map((category) => CategoryFilterModel(
            id: category.id,
            categoryModel: category,
            value: false,
          ))
      .toList();
}
