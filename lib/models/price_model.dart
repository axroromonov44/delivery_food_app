import 'package:equatable/equatable.dart';

class PriceModel extends Equatable {
  final int id;
  final String price;

  const PriceModel({
    required this.id,
    required this.price,
  });

  @override
  List<Object?> get props =>
      [
        id,
        price,
      ];

  static List<PriceModel> priceModel = [
    PriceModel(id: 1, price: '\$'),
    PriceModel(id: 2, price: '\$\$'),
    PriceModel(id: 3, price: '\$\$\$'),
  ];
}
