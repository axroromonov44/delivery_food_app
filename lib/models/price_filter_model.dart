import 'package:delivery_food_app/models/category_model.dart';
import 'package:delivery_food_app/models/price_model.dart';
import 'package:equatable/equatable.dart';

class PriceFilterModel extends Equatable {
  final int id;
  final PriceModel priceModel;
  final bool value;

  const PriceFilterModel({
    required this.id,
    required this.priceModel,
    required this.value,
  });

  PriceFilterModel copyWith({
    int? id,
    PriceModel? priceModel,
    bool? value,
  }) {
    return PriceFilterModel(
      id: id ?? this.id,
      priceModel: priceModel ?? this.priceModel,
      value: value ?? this.value,
    );
  }

  @override
  List<Object?> get props => [
        id,
        priceModel,
        value,
      ];

  static List<PriceFilterModel> filters = PriceModel.priceModel
      .map((priceModel) => PriceFilterModel(
            id: priceModel.id,
            priceModel: priceModel,
            value: false,
          ))
      .toList();
}
