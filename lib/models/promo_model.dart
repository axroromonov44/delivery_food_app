import 'package:equatable/equatable.dart';

class PromoModel extends Equatable {
  final int id;
  final String title;
  final String description;
  final String imgUrl;

  const PromoModel({
    required this.id,
    required this.title,
    required this.description,
    required this.imgUrl,
  });

  @override
  List<Object?> get props => [
        id,
        title,
        description,
        imgUrl,
      ];

  static List<PromoModel> promos = [
    PromoModel(
        id: 1,
        title: 'FREE Delivery on Your First 3 Orders.',
        description:
            "Place an order of \$10 or more and the delivery fee is on us",
        imgUrl: 'assets/cook.jpg'),
    PromoModel(
      id: 2,
      title: '20% off on Selected Restaurants',
      description: "Get a discount at more than 200+ restaurants",
      imgUrl: 'assets/cook.jpg',
    ),
  ];
}
