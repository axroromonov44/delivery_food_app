import 'package:delivery_food_app/models/menu_item_model.dart';
import 'package:equatable/equatable.dart';

class RestaurantModel extends Equatable {
  final int id;
  final String imageUrl;
  final String name;
  final List<String> tags;
  final List<MenuItemModel> menuItems;
  final int deliveryTime;
  final double deliveryFee;
  final double distance;

  const RestaurantModel({
    required this.id,
    required this.imageUrl,
    required this.menuItems,
    required this.name,
    required this.tags,
    required this.deliveryTime,
    required this.deliveryFee,
    required this.distance,
  });

  @override
  List<Object?> get props => [
        id,
        imageUrl,
        name,
        tags,
        deliveryTime,
        deliveryFee,
        distance,
        menuItems
      ];

  static List<RestaurantModel> restaurants = [
    RestaurantModel(
      id: 1,
      imageUrl: "assets/burak.jpg",
      name: "Milly Taomlar",
      tags: MenuItemModel.menuItems
          .where((menuItem) => menuItem.restaurantId == 1)
          .map((menuItem) => menuItem.category)
          .toSet()
          .toList(),
      deliveryTime: 30,
      deliveryFee: 3.99,
      distance: 0.7,
      menuItems: MenuItemModel.menuItems
          .where((element) => element.restaurantId == 1)
          .toList(),
    ),
    RestaurantModel(
      id: 2,
      imageUrl: "assets/burak.jpg",
      name: "CZN Burak",
      tags: MenuItemModel.menuItems
          .where((menuItem) => menuItem.restaurantId == 2)
          .map((menuItem) => menuItem.category)
          .toSet()
          .toList(),
      deliveryTime: 30,
      deliveryFee: 20,
      distance: 10,
      menuItems: MenuItemModel.menuItems
          .where((element) => element.restaurantId == 2)
          .toList(),
    ),
    RestaurantModel(
      id: 3,
      imageUrl: "assets/burak.jpg",
      name: "Nusret",
      tags: MenuItemModel.menuItems
          .where((menuItem) => menuItem.restaurantId == 3)
          .map((menuItem) => menuItem.category)
          .toSet()
          .toList(),
      deliveryTime: 30,
      deliveryFee: 2.99,
      distance: 0.1,
      menuItems: MenuItemModel.menuItems
          .where((element) => element.restaurantId == 3)
          .toList(),
    ),
    RestaurantModel(
      id: 4,
      imageUrl: "assets/burak.jpg",
      name: "Golden Ice Gelato Artigianale",
      tags: MenuItemModel.menuItems
          .where((menuItem) => menuItem.restaurantId == 4)
          .map((menuItem) => menuItem.category)
          .toSet()
          .toList(),
      deliveryTime: 30,
      deliveryFee: 5.7,
      distance: 0.28,
      menuItems: MenuItemModel.menuItems
          .where((element) => element.restaurantId == 4)
          .toList(),
    ),
  ];
}
