import 'package:delivery_food_app/models/category_filter_model.dart';
import 'package:delivery_food_app/models/price_filter_model.dart';
import 'package:equatable/equatable.dart';

class FilterModel extends Equatable {
  final List<CategoryFilterModel> categoryFilterModel;
  final List<PriceFilterModel> priceFilterModel;

  const FilterModel({
    this.categoryFilterModel = const <CategoryFilterModel>[],
    this.priceFilterModel = const <PriceFilterModel>[],
  });

  FilterModel copyWith({
    List<CategoryFilterModel>? categoryFilterModel,
    List<PriceFilterModel>? priceFilterModel,
  }) {
    return FilterModel(
      categoryFilterModel: categoryFilterModel ?? this.categoryFilterModel,
      priceFilterModel: priceFilterModel ?? this.priceFilterModel,
    );
  }

  @override
  List<Object?> get props => [
        categoryFilterModel,
        priceFilterModel,
      ];
}
