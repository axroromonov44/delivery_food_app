import 'package:equatable/equatable.dart';

class MenuItemModel extends Equatable {
  final int id;
  final int restaurantId;
  final String name;
  final String category;
  final String description;
  final double price;

  const MenuItemModel({
    required this.id,
    required this.restaurantId,
    required this.name,
    required this.category,
    required this.description,
    required this.price,
  });

  @override
  List<Object?> get props => [
        id,
        restaurantId,
        name,
        category,
        description,
        price,
      ];

  static List<MenuItemModel> menuItems = [
    const MenuItemModel(
        id: 1,
        restaurantId: 1,
        name: 'Qiyma Shashlik',
        category: "Shashlik",
        description: 'Pizza with Tomatoes',
        price: 5.99),
    const MenuItemModel(
        id: 2,
        restaurantId: 2,
        name: 'Coca cola',
        category: "Drinks",
        description: 'a cold beverage',
        price: 1.99),
    const MenuItemModel(
        id: 3,
        restaurantId: 3,
        name: 'Osh',
        category: "Palov",
        description: 'a cold beverage',
        price: 13.99),
    const MenuItemModel(
        id: 4,
        restaurantId: 4,
        name: 'Margherita',
        category: "Pizza",
        description: 'a cold beverage',
        price: 1),
  ];
}
