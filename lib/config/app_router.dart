import 'package:delivery_food_app/models/restaurant_model.dart';
import 'package:delivery_food_app/presentation/basket/basket_screen.dart';
import 'package:delivery_food_app/presentation/checkout/checkout_screen.dart';
import 'package:delivery_food_app/presentation/delivery_time/delivery_time_screen.dart';
import 'package:delivery_food_app/presentation/filter/filter_screen.dart';
import 'package:delivery_food_app/presentation/home/home_screen.dart';
import 'package:delivery_food_app/presentation/restaurant_details/restaurant_details_screen.dart';
import 'package:delivery_food_app/presentation/restaurant_lists/restaurant_lists_screen.dart';
import 'package:delivery_food_app/presentation/voucher/voucher_screen.dart';
import 'package:flutter/material.dart';

import '../presentation/location/location_screen.dart';

class AppRouter {
  static Route? onGenerateRoute(RouteSettings settings) {
    print('The Route is: ${settings.name}');

    switch (settings.name) {
      case '/':
        return HomeScreen.route();
      case HomeScreen.routeName:
        return HomeScreen.route();
      case LocationScreen.routeName:
        return LocationScreen.route();
      case BasketScreen.routeName:
        return BasketScreen.route();
      case CheckoutScreen.routeName:
        return CheckoutScreen.route();
      case DeliveryTimeScreen.routeName:
        return DeliveryTimeScreen.route();
      case FilterScreen.routeName:
        return FilterScreen.route();
      case RestaurantDetailsScreen.routeName:
        return RestaurantDetailsScreen.route(
            restaurantModel: settings.arguments as RestaurantModel);
      case RestaurantListsScreen.routeName:
        return RestaurantListsScreen.route(
          restaurantModel: settings.arguments as List<RestaurantModel>
        );
      case VoucherScreen.routeName:
        return VoucherScreen.route();

      default:
        return _errorRoute();
    }
  }

  static Route _errorRoute() {
    return MaterialPageRoute(
      builder: (_) => Scaffold(
        appBar: AppBar(
          title: const Text('error'),
        ),
      ),
      settings: const RouteSettings(name: '/error'),
    );
  }
}
