import 'package:delivery_food_app/bloc/auto_complete/autocomplete_bloc.dart';
import 'package:delivery_food_app/bloc/geolocation/geolocation_bloc.dart';
import 'package:delivery_food_app/bloc/place/place_bloc.dart';
import 'package:delivery_food_app/config/app_router.dart';
import 'package:delivery_food_app/config/theme.dart';
import 'package:delivery_food_app/presentation/home/home_screen.dart';
import 'package:delivery_food_app/presentation/location/location_screen.dart';
import 'package:delivery_food_app/repositories/geolocation/geolocation_repository.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

import 'repositories/places/places_repository.dart';

void main() {
  if (defaultTargetPlatform == TargetPlatform.android) {
    AndroidGoogleMapsFlutter.useAndroidViewSurface = true;
  }
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiRepositoryProvider(
      providers: [
        RepositoryProvider<GeolocationRepository>(
          create: (_) => GeolocationRepository(),
        ),
        RepositoryProvider<PlacesRepository>(
          create: (_) => PlacesRepository(),
        ),
      ],
      child: MultiBlocProvider(
        providers: [
          BlocProvider(
            create: (context) => GeolocationBloc(
              geolocationRepository: context.read<GeolocationRepository>(),
            )..add(LoadGeolocationEvent()),
          ),
          BlocProvider(
            create: (context) => AutocompleteBloc(
              repository: context.read<PlacesRepository>(),
            )..add(LoadAutocompleteEvent()),
          ),
          BlocProvider(
            create: (context) => PlaceBloc(
              repository: context.read<PlacesRepository>(),
            ),
          )
        ],
        child: MaterialApp(
          title: 'Delivery Food',
          theme: theme(),
          onGenerateRoute: AppRouter.onGenerateRoute,
          initialRoute: HomeScreen.routeName,
          // home: const HomeScreen(),
        ),
      ),
    );
  }
}
