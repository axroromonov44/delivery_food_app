import 'package:delivery_food_app/models/place_model.dart';

abstract class BasePlacesRepository {
  Future<List?> getAutocomplete(String searchInput);

  Future<Place?> getPlace(String placeId)async { }
}
