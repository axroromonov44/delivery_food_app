import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:delivery_food_app/models/place_model.dart';
import 'package:delivery_food_app/repositories/places/places_repository.dart';
import 'package:equatable/equatable.dart';

part 'place_event.dart';

part 'place_state.dart';

class PlaceBloc extends Bloc<PlaceEvent, PlaceState> {
  final PlacesRepository _repository;
  StreamSubscription? _streamSubscription;

  PlaceBloc({required PlacesRepository repository})
      : _repository = repository,
        super(PlaceLoadingState()) {
    on<LoadPlaceEvent>((event, emit) async {
      emit(PlaceLoadingState());
      try {
        _streamSubscription?.cancel();
        final Place place = await _repository.getPlace(event.placeId);
        print(event);
        print(state);
        emit(PlaceLoadedState(place: place));
      } catch (e) {}
    });
  }

  @override
  Future<void> close() {
    _streamSubscription?.cancel();
    return super.close();
  }
}
