part of 'place_bloc.dart';

abstract class PlaceState extends Equatable {
  const PlaceState();
}

class PlaceLoadingState extends PlaceState {
  @override
  List<Object> get props => [];
}

class PlaceLoadedState extends PlaceState {
  final Place place;

  const PlaceLoadedState({
    required this.place,
  });

  @override
  List<Object> get props => [place];
}

class PlaceErrorState extends PlaceState {
  @override
  List<Object> get props => [];
}
