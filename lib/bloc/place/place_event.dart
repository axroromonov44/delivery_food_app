part of 'place_bloc.dart';

abstract class PlaceEvent extends Equatable {
  const PlaceEvent();
}

class LoadPlaceEvent extends PlaceEvent{
  final String placeId;

  const LoadPlaceEvent({required this.placeId});

  @override
  // TODO: implement props
  List<Object?> get props => [placeId];

}
