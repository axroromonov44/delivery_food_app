part of 'filters_bloc.dart';

abstract class FiltersState extends Equatable {
  const FiltersState();
}

class FiltersLoading extends FiltersState {
  @override
  List<Object> get props => [];
}

class FiltersLoaded extends FiltersState {
  final FilterModel filterModel;

  const FiltersLoaded({
    this.filterModel = const FilterModel(),
  });

  @override
  List<Object?> get props => [filterModel];
}
