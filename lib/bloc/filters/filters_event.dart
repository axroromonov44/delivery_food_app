part of 'filters_bloc.dart';

abstract class FiltersEvent extends Equatable {
  const FiltersEvent();
}

class FiltersLoadEvent extends FiltersEvent {
  @override
  List<Object?> get props => [];
}

class CategoryFiltersUpdated extends FiltersEvent {
  final CategoryFilterModel categoryFilterModel;

  const CategoryFiltersUpdated({
    required this.categoryFilterModel,
  });

  @override
  List<Object?> get props => [categoryFilterModel];
}

class PriceFiltersUpdated extends FiltersEvent {
  final PriceFilterModel priceFilterModel;

  const PriceFiltersUpdated({
    required this.priceFilterModel,
  });

  @override
  List<Object?> get props => [priceFilterModel];
}
