import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:delivery_food_app/models/category_filter_model.dart';
import 'package:delivery_food_app/models/filter_model.dart';
import 'package:delivery_food_app/models/price_filter_model.dart';
import 'package:equatable/equatable.dart';

part 'filters_event.dart';

part 'filters_state.dart';

class FiltersBloc extends Bloc<FiltersEvent, FiltersState> {
  FiltersBloc() : super(FiltersLoading()) {
    on<FiltersLoadEvent>((event, emit) {});
  }
}
