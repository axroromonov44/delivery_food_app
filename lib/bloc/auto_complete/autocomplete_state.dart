part of 'autocomplete_bloc.dart';

abstract class AutocompleteState extends Equatable {
  const AutocompleteState();
}

class AutocompleteLoadingState extends AutocompleteState {
  @override
  List<Object> get props => [];
}

class AutocompleteLoadedState extends AutocompleteState {
  final List<PlaceAutocomplete> autocomplete;

  const AutocompleteLoadedState({required this.autocomplete});

  @override
  List<Object> get props => [autocomplete];
}

class AutocompleteErrorState extends AutocompleteState {
  @override
  List<Object> get props => [];
}
