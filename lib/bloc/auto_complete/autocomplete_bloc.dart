import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:delivery_food_app/models/place_autocomplete_model.dart';
import 'package:delivery_food_app/repositories/places/places_repository.dart';
import 'package:equatable/equatable.dart';

part 'autocomplete_event.dart';

part 'autocomplete_state.dart';

class AutocompleteBloc extends Bloc<AutocompleteEvent, AutocompleteState> {
  final PlacesRepository placesRepository;
  StreamSubscription? streamSubscription;

  AutocompleteBloc({required PlacesRepository repository})
      : placesRepository = repository,
        super(AutocompleteLoadingState()) {
    on<LoadAutocompleteEvent>((event, emit) async {
      streamSubscription?.cancel();
      final List<PlaceAutocomplete> autocomplete =
          await placesRepository.getAutocomplete(event.searchInput);
      emit(AutocompleteLoadedState(autocomplete: autocomplete));
    });
  }
}
